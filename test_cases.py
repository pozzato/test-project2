import pytest
import selenium
from selenium import webdriver
from selenium.webdriver.chrome.options import Options



class TestCases():
    
    @pytest.fixture(scope="module")
    def driver(self):
        options = Options()
        options.add_argument('--no-sandbox')
        options.add_argument('--window-size=1420,1080')
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        
        driver = webdriver.Chrome(options=options)
        driver.get("https://the-internet.herokuapp.com/context_menu")
        
        yield driver
        
        driver.close()
        driver.quit()



    # Test Case 1 checking the full Page Source
    def test_case_1(self,driver):
        
        text = "Right-click in the box below to see one called 'the-internet'"
        assert text in driver.page_source


    # Test Case 2 checking the full Page Source
    def test_case_2(self,driver):
        text = "Alibaba"
        assert text in driver.page_source
        
        
#     # Test Case 1 checking all the <p> tags from the page
#     def test_case_1(self,driver):
        
#         flag = False
#         text = "Right-click in the box below to see one called 'the-internet'"
#         list_elements = driver.find_elements_by_tag_name("p")
        
#         for e in list_elements:
#             if text in e.text:
#                 flag = True
#                 break
        
#         assert flag

#   # Test Case 2 checking all the <p> tags from the page
#     def test_case_2(self,driver):
        
#         flag = False
#         text = "Alibaba"
#         list_elements = driver.find_elements_by_tag_name("p")
        
#         for e in list_elements:
#             if text in e.text:
#                 flag = True
#                 break
        
#         assert flag

